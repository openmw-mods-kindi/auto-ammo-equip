local types = require("openmw.types")
local this = {}
local getRecord = types.Weapon.record

this.ammoType = {
    [types.Weapon.TYPE.MarksmanBow] = types.Weapon.TYPE.Arrow,
    [types.Weapon.TYPE.MarksmanCrossbow] = types.Weapon.TYPE.Bolt
}

local function getAverageAmmoDamage(ammo)
    -- marksman weapons uses chop, chop, chop attack
    local record = getRecord(ammo)
    local averageDmg = (record.chopMaxDamage + record.chopMinDamage) / 2
    return averageDmg
end

local function compEnch(a, b, negate)
    local enchrecA = getRecord(a).enchant
    local enchrecB = getRecord(b).enchant
    return (enchrecA ~= nil and enchrecB == nil) == negate
end

local function compVal(a, b, negate)
    return (getRecord(a).value < getRecord(b).value) == negate
end

local function compDmg(a, b, negate)
    return (getAverageAmmoDamage(a) < getAverageAmmoDamage(b)) == negate
end

this.defCompOrder = {
    compEnch,
    compVal,
    compDmg
}
this.comp = {
    compEnch,
    compVal,
    compDmg,
    true,
    true,
    true
}

return this
