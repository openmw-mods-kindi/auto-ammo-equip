local self = require("openmw.self")
local types = require("openmw.types")
local aux_util = require("openmw_aux.util")
local core = require("openmw.core")
local ui = require("openmw.ui")
local async = require("openmw.async")
local input = require("openmw.input")
local I = require("openmw.interfaces")

local modInfo = require("scripts.auto_ammo_equip_omw.modInfo")
local constants = require("scripts.auto_ammo_equip_omw.constants")
local playerSettings = require("openmw.storage").playerSection("Settings_AAEOMW_Options_Key_KINDI")

local equippedWeapon = nil -- currently equipped weapon
local lastEquippedWeapon = nil -- handles thrown weapons

local defCompOrder = constants.defCompOrder
local comp = constants.comp
local ammoType = constants.ammoType

local function getRecord(obj)
    return obj.type.record(obj)
end

local function updatePriority()
    local order = playerSettings:get("Equip Priority") or "1,2,3"
    local pos = 1
    local priority = {}
    for index in string.gmatch(order, "(-?%d+),?") do
        local i = tonumber(index)
        if type(i) == "number" then
            table.insert(priority, i)
        else
            return
        end
    end
    for k, index in ipairs(priority) do
        comp[pos] = defCompOrder[math.abs(index)]
        comp[#defCompOrder + pos] = true
        if index < 0 then
            comp[#defCompOrder + pos] = false
        end
        pos = pos + 1
    end
end

playerSettings:subscribe(async:callback(function(sectionName, changedKey)
    if sectionName == "Settings_AAEOMW_Options_Key_KINDI" then
        if changedKey == "Equip Priority" or changedKey == nil then
            pcall(updatePriority) -- protected call just in case something breaks during setting subscription
        end
    end
end))

local function ammoSorter(ammunitions)
    table.sort(ammunitions, function(a, b)
        return comp[3](a, b, comp[6])
    end)
    table.sort(ammunitions, function(a, b)
        return comp[2](a, b, comp[5])
    end)
    table.sort(ammunitions, function(a, b)
        return comp[1](a, b, comp[4])
    end)
end

local function getAmmo(ammoTypeToEquip)
    local inv = types.Actor.inventory(self)
    local ammunitions = inv:getAll(types.Weapon)

    ammunitions = aux_util.mapFilter(ammunitions, function(ammo)
        return getRecord(ammo).type == ammoTypeToEquip
    end)

    if next(ammunitions) then
        ammoSorter(ammunitions)
        return ammunitions[1]
    end
end

local function autoEquip(ammoTypeToEquip, override)
    if playerSettings:get("Attack Stance") and types.Actor.getStance(self) ~= types.Actor.STANCE.Weapon then
        return
    end

    local newAmmo = override or getAmmo(ammoTypeToEquip)
    if newAmmo then
        core.sendGlobalEvent("UseItem", {
            object = newAmmo,
            actor = self,
            force = true
        })
        -- due to delayed UseItem, notification can spam.. A workaround is to use setEquipment but it won't trigger handlers
        -- so for now, we resort to using empty strings to drown the spams
        if playerSettings:get("Notification") then
            ui.showMessage(string.format(core.getGMST("sLoadingMessage15") .. " %ss..", newAmmo.count, newAmmo.type.record(newAmmo).name))
            ui.showMessage("")
            ui.showMessage("")
        end
    end
end

local function alreadyEquipped(ammoTypeToEquip)
    local equippedAmmo = types.Actor.getEquipment(self, types.Actor.EQUIPMENT_SLOT.Ammunition)
    return equippedAmmo and getRecord(equippedAmmo).type == ammoTypeToEquip
end

local function wasDropped(obj)
    -- check if the obj was dropped to the ground/into world
    -- check if the obj went into another inventory
    return obj.cell or obj.parentContainer ~= self.object
end

local function onUpdate()
    if not playerSettings:get("Mod Status") then
        return
    end

    local inv = types.Actor.inventory(self)
    equippedWeapon = types.Actor.getEquipment(self, types.Actor.EQUIPMENT_SLOT.CarriedRight)
    lastEquippedWeapon = equippedWeapon or lastEquippedWeapon

    if equippedWeapon then
        local ammoTypeToEquip = ammoType[getRecord(equippedWeapon).type]
        if ammoTypeToEquip then
            if not alreadyEquipped(ammoTypeToEquip) then
                autoEquip(ammoTypeToEquip)
            end
        end
    elseif lastEquippedWeapon then -- if no weapon is currently equipped, determine if the last weapon was a thrown weapon
        if getRecord(lastEquippedWeapon).type == types.Weapon.TYPE.MarksmanThrown then
            if not inv:find(lastEquippedWeapon.recordId) and not wasDropped(lastEquippedWeapon) then
                autoEquip(types.Weapon.TYPE.MarksmanThrown)
            end
        end
    end
end

local function nextAmmo(list, dir, isThrown)
    local typeToSearch = isThrown and types.Actor.EQUIPMENT_SLOT.CarriedRight or types.Actor.EQUIPMENT_SLOT.Ammunition
    local equippedAmmo = types.Actor.getEquipment(self)[typeToSearch]

    for i, a in ipairs(list) do
        if equippedAmmo.recordId == a.recordId then
            local next = list[i + dir]
            if next then
                return next
            end
            if dir > 0 then
                return list[1]
            end
            return list[#list]
        end
    end
end

local function onMouseWheel(v, h)
    if equippedWeapon and playerSettings:get("Quick Switch") then
        local inv = types.Actor.inventory(self)
        local isThrown = getRecord(equippedWeapon).type == types.Weapon.TYPE.MarksmanThrown
        local ammoT = isThrown and types.Weapon.TYPE.MarksmanThrown or ammoType[getRecord(equippedWeapon).type]

        if ammoT and input.isAltPressed() then
            local allAmmo = aux_util.mapFilter(inv:getAll(types.Weapon), function(wpn)
                return getRecord(wpn).type == ammoT
            end)
            ammoSorter(allAmmo)
            autoEquip(nil, nextAmmo(allAmmo, v, isThrown))
        end
    end
end

local function onFrame()
    local isMMMode = equippedWeapon and (ammoType[getRecord(equippedWeapon).type] or getRecord(equippedWeapon).type == types.Weapon.TYPE.MarksmanThrown)
    if playerSettings:get("Quick Switch") and playerSettings:get("Mod Status") and isMMMode and input.isAltPressed() then
        I.Camera.disableZoom("AUTO_AMMO_EQUIP_OMW")
    else
        I.Camera.enableZoom("AUTO_AMMO_EQUIP_OMW")
    end
end

return {
    engineHandlers = {
        onActive = function()
            assert(core.API_REVISION >= modInfo.MIN_API, string.format("[%s] mod requires Lua API %s or newer!", modInfo.MOD_NAME, modInfo.MIN_API))
        end,
        onUpdate = onUpdate,
        onMouseWheel = onMouseWheel,
        onFrame = onFrame
    },
    eventHandlers = {
        AAE_MarksmanWeaponEquipped_eqnx = function(data)
            if not alreadyEquipped(data.ammoType) then
                autoEquip(data.ammoType)
            end
        end
    }
}
