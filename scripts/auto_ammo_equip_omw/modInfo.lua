local CHANGES = [[
    Added quick ammo switch using mousewheel
]]

return setmetatable({
    MOD_NAME = "Auto Ammo Equip",
    MOD_VERSION = 1.0,
    MIN_API = 60,
    CHANGES = CHANGES
}, {
    __tostring = function(modInfo)
        return string.format("\n[%s]\nVersion: %s\nMinimum API: %s\nChanges: %s", modInfo.MOD_NAME, modInfo.MOD_VERSION,
            modInfo.MIN_API, modInfo.CHANGES)
    end,
    __metatable = tostring
})

-- require("scripts.auto_ammo_equip_omw.modInfo")
