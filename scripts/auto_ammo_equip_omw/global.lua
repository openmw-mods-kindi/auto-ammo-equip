local I = require("openmw.interfaces")
local types = require("openmw.types")
local world = require("openmw.world")
local modInfo = require("scripts.auto_ammo_equip_omw.modInfo")
local constants = require("scripts.auto_ammo_equip_omw.constants")

local ammoType = constants.ammoType

-- we use handler so equipping can be blocked by other mods
I.ItemUsage.addHandlerForType(types.Weapon, function(weapon, actor)
    local type = ammoType[types.Weapon.record(weapon).type]
    if type then
        actor:sendEvent("AAE_MarksmanWeaponEquipped_eqnx", {
            ammoType = type
        })
    end

end)

return {
    interfaceName = "AutoAmmoEquip_eqnx",
    interface = {
        info = tostring(modInfo),
        help = tostring([[Nothing to help]]),
        test = function()
            world.createObject("steel crossbow", 1):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("steel bolt", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("iron bolt", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("silver bolt", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("cruel flame bolt", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("dire flame bolt", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("cruel viper bolt", 5):moveInto(types.Actor.inventory(world.players[1]))

            world.createObject("steel longbow", 1):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("steel arrow", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("iron arrow", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("silver arrow", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("arrow of wasting flame", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("cruel sparkarrow", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("flame arrow", 5):moveInto(types.Actor.inventory(world.players[1]))

            world.createObject("steel dart", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("ebony dart", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("silver dart", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("cruel flamestar", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("cruel sparkstar", 5):moveInto(types.Actor.inventory(world.players[1]))
            world.createObject("flying viper", 5):moveInto(types.Actor.inventory(world.players[1]))
        end
    }
}
